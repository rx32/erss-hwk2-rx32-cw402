#ifndef __CACHE_H__
#define __CACHE_H__

#include <mutex>
#include <unordered_map>
#include <list>
#include "request.h"
#include "response.h"
#include <iostream>

#define cache_size 256

std::mutex mtx;

class Cache{
private:
	std::unordered_map<std::string,Response> map;
	std::list<std::string> list;
	
public:
	Cache(){}
		
	//call this method to update cache
	void updateCache(const std::string & url, const Response & response){
		std::unique_lock<std::mutex> lck(mtx);
		if(map.find(url)==map.end()){
			//here we need to insert a new entry into our cache
			if(cache_size==list.size()){//remove the least recently used element inside the cache
				std::string last = list.back();
				list.pop_back();
				map.erase(last);
			}	
			map[url] = response;
			list.push_front(url);
		}else{
			//here we need to update a current entry of our cache
			list.remove(url);
    	list.push_front(url);
			map[url] = response;
		}
	}
	//check if we have corresponding element
	bool contains(const std::string & url){
		std::unique_lock<std::mutex> lck(mtx);
		if(map.find(url)!=map.end()){
			return true;
		}else{
			return false;
		}
	}
	//get the result out of cache, if url is not inside cache, nothing will happen
	 
	Response & getResponse(const std::string & url){
		std::unique_lock<std::mutex> lck(mtx);
		if(map.find(url)==map.end()){
			throw invalid_argument("key doesn't exist inside map");
		}else{
			//update lru
			list.remove(url);
			list.push_front(url);
			return map.at(url);
		}
	}
};


#endif 
