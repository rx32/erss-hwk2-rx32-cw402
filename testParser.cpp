#include <iostream>
#include "cache.h"
#include "request.cpp"
#include "response.cpp"
#include <string>

using namespace std;


int main(){
     string test1 = "GET /uri.cgi HTTP/1.1\r\n"
                        "User-Agent: Mozilla/5.0\r\n"
                        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n"
                        "Host: www.google.com\r\n"
                        "\r\nthis is some data";

    string test2 = "HTTP/1.1 200 OK\r\n"
            "Server: nginx/1.2.1\r\n"
            "Content-Type: text/html\r\n"
            "Content-Length: 8\r\n"
            "Connection: keep-alive\r\n"
            "\r\n"
            "<html />";
   Request msg1(test1);
   cout<<"host name is:"<<msg1.getHostName()<<endl;
   cout<<"port is: "<<msg1.getPort()<<endl;
   cout<<"request body is:"<<msg1.getData()<<endl; 
			

	 Response msg2(test2);
   cout<<"server name is:"<<msg2.getField("Server")<<endl;
   bool exist = false;
   int len = 999;
   exist = msg2.getContentLen(len);
   if(exist) cout<<"Content length is:"<<len<<endl;
	 cout<<"response body is:"<<msg2.getData()<<endl;
}
