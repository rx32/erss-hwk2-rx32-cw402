//
// Created by ruji on 2/22/20.
//
#include <cstring>
#include <netdb.h>
#include <iostream>
#include <utility>
#include "client.h"
#include <netinet/in.h>
#include <arpa/inet.h>

#define DEBUG 1

class Client {

protected:
//  int http_server_fd{};
  std::string received_data;

public:
  explicit Client() = default;

  int connect_to_server(const std::string& hostname, const std::string& port) {
    const char * http_port = port.c_str();
    hostent* he = gethostbyname(hostname.c_str());

    if (he == nullptr) {
      // TODO: should return 404 or some error infomation
      return 0;
    }

    std::cout << "Official name is: " << he->h_name << std::endl;
    std::cout << "IP addresses: " << std::endl;
    struct in_addr **addr_list = (struct in_addr **)he->h_addr_list;
    for(int i = 0; addr_list[i] != NULL; i++) {
      std::cout << inet_ntoa(*addr_list[i]) << std::endl;
    }

    const char * target_server_ip = inet_ntoa(*addr_list[0]);

    int status;
    struct addrinfo host_info{};
    struct addrinfo *host_info_list;

    memset(&host_info, 0, sizeof(host_info));
    host_info.ai_family   = AF_UNSPEC;
    host_info.ai_socktype = SOCK_STREAM;

    status = getaddrinfo(target_server_ip, http_port, &host_info, &host_info_list);
    if (status != 0) {
      std::cerr << "Error: cannot get address info for host" << std::endl;
      std::cerr << "  (" << target_server_ip << "," << http_port << ")" << std::endl;
      return -1;
    }

    int http_server_fd = socket(host_info_list->ai_family,
                           host_info_list->ai_socktype,
                           host_info_list->ai_protocol);
    if (http_server_fd == -1) {
      std::cerr << "Error: cannot create socket" << std::endl;
      std::cerr << "  (" << target_server_ip << "," << http_port << ")" << std::endl;
      return -1;
    }

    if (DEBUG) {
      std::cout << "Connecting to " << target_server_ip << " on port " << http_port << "..." << std::endl;
    }

    status = connect(http_server_fd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if (status == -1) {
      std::cerr << "Error: cannot connect to socket" << std::endl;
      std::cerr << "  (" << target_server_ip << "," << http_port << ")" << std::endl;
      return -1;
    }

    freeaddrinfo(host_info_list);

    return http_server_fd;
  }

//  int receive_from_server() {
//    char buffer[65536];
//    int size = recv(http_server_fd, buffer, sizeof(buffer), 0);
//    std::cout << "------> size received from server: " << size << std::endl;
//    buffer[size] = '\0';
//    received_data = std::string(buffer, size);
//    std::cout << "------> data received from server: " << std::endl;
//    std::cout << buffer << std::endl;
//    return size;
//  }


};
