CFLAGS=--std=c++11 -pedantic -Wall -Werror -ggdb3 -pthread

all: proxy_worker

proxy_worker: proxy_worker.cpp client.cpp client.h httpmsg.h request.h response.h server.cpp server.h Makefile
	g++ -o $@ $(CFLAGS) proxy_worker.cpp


clean:
	rm -f proxy_worker *.o *.so
