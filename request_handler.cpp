//
// Created by ruji on 2/23/20.
//

class RequestHandler {
private:

public:
  void serve() {
    while (true) {
      int client_browser_fd = accept_connection();
      std::thread t(&Proxy::handle_request, this, client_browser_fd);
      t.detach();
    }
  }
};