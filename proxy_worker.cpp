
#include <utility>
#include <thread>

#include "server.cpp"
#include "client.cpp"
#include "cache.h"
#include "logger.hpp"

#define DEBUG 1

class ProxyWorker : public Server, public Client {

  Cache cache;

public:

  explicit ProxyWorker(const char * listen_port) : Server(listen_port), cache() {}

  int send_all(const char * data, int  total_size, int target_fd) {
    int sent_size = 0;
    while (sent_size != total_size) {
      int single_sent_size = send(target_fd, data + sent_size, total_size - sent_size, 0);
      if (single_sent_size == -1) {
        return -1;
      }
      sent_size += single_sent_size;
    }
    return sent_size;
  }

  void forward_to_server(const char * header, int http_server_fd) {
    std::cout << "------> data forward to server: " << std::endl;
    std::cout << header << std::endl;
    int size = send_all(header, strlen(header), http_server_fd);
    std::cout << "------> size forward to server: " << size << std::endl;
  }

//  void forward_to_client() {
//    std::cout << "------> data forward to client: " << std::endl;
//    std::cout << "-----> data" << std::endl;
//    std::cout << received_data << std::endl;
//    int size = send_all(received_data, connection_fd);
//    std::cout << "------> forward size: " << size << std::endl;
//  }

  int receive_and_forward_chunk(int http_server_fd, int client_connection_fd) {
    char buffer[2048];
    while (1) {
      memset(buffer, '\0', 2048);
      int size = recv(http_server_fd, buffer, sizeof(buffer), 0);
      if (size == 0 || size == -1) {
        close(http_server_fd);
        close(client_connection_fd);
        break;
      }
      std::cout << "data_size: " << size << std::endl;
      std::cout << "buffer: " << buffer << std::endl;
      std::cout << "-----------------------------------------------------------" << std::endl;
      send_all(buffer, size, client_connection_fd);
      if (buffer[0] == '0') {
        break;
      }
    }
    return 0;
  }

  int serve() {
    init_server();
    while (true) {
      int client_browser_fd = accept_connection();
      std::thread t(&ProxyWorker::handle_request, this, client_browser_fd);
      t.detach();
    }
    return 0;
  }

  void receive_and_forward(int http_server_fd, int client_connection_fd, Request& request, int request_uid) {
    std::cout << "start to receive and forward" << std::endl;
    char buffer[2048];
    int size = recv(http_server_fd, buffer, sizeof(buffer), 0);
    if (size == 0 || size == -1) {
      close(http_server_fd);
      close(client_connection_fd);
      return;
    }
    std::string header(buffer, size);
    std::vector<char> header_vec;
    for (char c : header) {
      header_vec.push_back(c);
    }
    Response response(header_vec);
    Logger::getLogger()->logRecvResponse(request_uid, request, response);

    send(client_connection_fd, buffer, size, 0);
    receive_and_forward_directly(http_server_fd, client_connection_fd);
    //    if (header.find("Content-Length") != std::string::npos) {
//      std::cout << "---------------forward" << std::endl;
//      std::string pattern = "Content-Length: ";
//      int pattern_idx = header.find(pattern);
//      int length_end = header.find("\r\n", pattern_idx);
//      int content_length = std::stoi(header.substr(pattern_idx + pattern.length(), length_end - (pattern_idx + pattern.length())));
//      int content_end = header.find("\r\n\r\n");
//      content_length = content_length - (size - content_end - 4);
//      receive_and_forward_content_length(http_server_fd, client_connection_fd, content_length);
//    } else if (header.find("chunked") != std::string::npos) {
//      std::cout << "---------------chunked data" << std::endl;
//      receive_and_forward_chunk(http_server_fd, client_connection_fd);
//    } else {
//      std::cout << "---------------directly forward" << std::endl;
//      receive_and_forward_directly(http_server_fd, client_connection_fd);
//    }
  }

  void receive_and_forward_content_length(int http_server_fd, int client_connection_fd, int content_length) {
    char buffer[2048];
    int sent_size = 0;
    std::cout << "content_length: " << content_length << std::endl;
    while(sent_size < content_length) {
      int size = recv(http_server_fd, buffer, sizeof(buffer), 0);
      if (size == -1 || size == 0 ){
        close(http_server_fd);
        close(client_connection_fd);
        return;
      }
      std::cout << "size: " << size << std::endl;

      sent_size += size;
      send(client_connection_fd, buffer, size, 0);
    }
  }

  void receive_and_forward_directly(int http_server_fd, int client_connection_fd) {
    char buffer[2048];
    while (true) {
      int size = recv(http_server_fd, buffer, sizeof(buffer), 0);
      if (size == -1 || size == 0) {
        close(client_connection_fd);
        close(http_server_fd);
        return;
      }
      send(client_connection_fd, buffer, size, 0);
    }

  }

  void run_connection(int http_server_fd, int client_connection_fd, int request_uid) {
    cout<<"successfully connect to server"<<endl;
    //send 200 ok to client
    const char * success_msg  = "200 ok";
    size_t size = strlen(success_msg)+1;
    send(client_connection_fd,success_msg,size,0);
    cout<<"start running connection!"<<endl;
    //set vector for all file descriptors
    vector<int> fds;
    fds.clear();
    fds.push_back(client_connection_fd);
    fds.push_back(http_server_fd);
    int max_fd = fds[0]>fds[1]?fds[0]:fds[1];
    while(true){//break if we receive an empty msg
      //prepare the buffer we need
      char buffer[65536];
      memset(buffer,'\0',sizeof(buffer));
      cout<<"In the begining buffer is "<<buffer<<endl;
      //set the readfds we need
      fd_set readfds;
      FD_ZERO(&readfds);
      cout<<"FD zero"<<buffer<<endl;
      FD_SET(client_connection_fd, &readfds);
      cout<<"FD_SET"<<buffer<<endl;
      FD_SET(http_server_fd, &readfds);
      cout<<"FD_SET"<<buffer<<endl;
      cout<<"max_fd: " << max_fd <<endl;
      int status = select(max_fd+1, &readfds, NULL, NULL, NULL);
      if (status == -1) {
        return;
      }
      cout<<"After select"<<buffer<<endl;
      //check which file descriptor receive the message
      for(size_t i=0;i<fds.size();i++){
        size_t send_fd = (0==i)?fds[1]:fds[0];
        if(FD_ISSET(fds[i], &readfds)){
          int msg_len = recv(fds[i],buffer,sizeof(buffer),0);
          if (msg_len == 0 || msg_len == -1) {
            close(client_connection_fd);
            close(http_server_fd);
            Logger::getLogger()->logCloseTunnel(request_uid);
            cout<<"here I close this tunnel"<<endl;
            return;
          }
          string msg(buffer);
          if(0==i){
            cout<<"Here I receive a msg from client, msg is:"<<endl;
            cout<<msg<<endl;
          }else{
            cout<<"Here I receive a msg from server, msg is:"<<endl;
            cout<<msg<<endl;
          }
          cout<<"here I send the message"<<endl;
          send(send_fd,buffer,msg_len,0);
        }
      }
    }
  }


  void handle_request(int client_browser_df) {
    Logger * logger = Logger::getLogger();
    const char * header;
    try {
      header = receive_header(client_browser_df);
    } catch (const char * e) {
      close(client_browser_df);
      return;
    }

    std::string header_str(header);
    std::vector<char> header_vec(header, header + strlen(header));
    Request request(header_vec);

    int request_uid = logger->logNewReq(request);

    std::string hostname = request.getHostName();
    std::string port = request.getPort();
    int http_server_fd = connect_to_server(hostname, port);

    if (DEBUG) {
      std::cout << "[CACHE] hostname: " << hostname << std::endl;
      if (hostname.empty()) {
        close(client_browser_df);
        std::cerr << "[CACHE] cannot parse host name" << std::endl;
        return;
      }
    }

    if (request.getMethodName() == "CONNECT") {
      run_connection(http_server_fd, client_browser_df, request_uid);

      if (DEBUG) {
        std::cout << "[CACHE] This is CONNECT reqeust." << std::endl;
      }

      close(client_browser_df);
      close(http_server_fd);
    } else if (request.getMethodName() == "GET") {
      handle_request_with_cache(client_browser_df, http_server_fd, header_vec, request_uid);
    } else {
      forward_to_server(header, http_server_fd);
      receive_and_forward(http_server_fd, client_browser_df, request, request_uid);
      close(http_server_fd);
      close(client_browser_df);
    }
  }

  int handle_request_with_cache(int client_browser_fd, int http_server_fd, vector<char> request_header, int request_uid) {

    Request request(request_header);

    Response response;
    std::string url = request.getUrl();
    if (!cache.contains(url)) {
      Logger::getLogger()->logNotInCache(request_uid);
      std::cout << "[CACHE] begin send request with cache!" << std::endl;
      send_request_with_cache(http_server_fd, request, request_uid);
      try {
        response = recv_response_with_cache(http_server_fd, client_browser_fd, request_uid);
        std::cout << "[CACHE] response completely received!" << std::endl;
      } catch(const char * e) {
        close(client_browser_fd);
        throw e;
      }
      Logger::getLogger()->logRecvResponse(request_uid, request, response);
      std::cout << "[CACHE] try to update cache" << std::endl;
      try_update(url, response);
      std::cout << "[CACHE] *****************send response**********************" << std::endl;
      send_response(client_browser_fd, response, request_uid);
    } else {
      response = cache.getResponse(url);
      Logger::getLogger()->logInCache(request_uid, response);
      std::cout << "[CACHE] url: " << url << "response is in the cache." << std::endl;
      if (response.mustReValidate()) {
        std::cout << "[CACHE] response must be revalidated" << std::endl;
        add_header(request, response);
        receive_and_forward_with_cache(request, url, http_server_fd, client_browser_fd, request_uid);
      } else if (response.isFresh()) {
//      } else if (false) {
        std::cout << "[CACHE] response is fresh" << std::endl;
        send_response(client_browser_fd, response, request_uid);
        std::cout << "[CACHE] Successfully get response from cache!" << std::endl;
      } else if (response.isVerifiable()) {
        std::cout << "[CACHE] response is verifiable" << std::endl;
        add_header(request, response);
        receive_and_forward_with_cache(request, url, http_server_fd, client_browser_fd, request_uid);
      } else {
        std::cout << "[CACHE] all false!" << std::endl;
        receive_and_forward_with_cache(request, url, http_server_fd, client_browser_fd, request_uid);
      }
    }
    return 0;
  }

  void add_header(Request& request, Response& response) {
    std::string etag = response.getField("ETag");
    std::cout << "[CACHE] etag: " << etag << std::endl;
    if (!etag.empty()) {
      std::cout << "[CACHE] etag is not empty!" << std::endl;
      request.addHeader("If-None-Match", response.getField("ETag"));
    }
    std::string last_modified = response.getField("Last-Modified");
    std::cout << "[CACHE] last modify: " << last_modified << std::endl;
    if (!last_modified.empty()) {
      std::cout << "[CACHE] last_modified is not empty!" << std::endl;
      request.addHeader("If-Last-Modify", response.getField("Last-Modified"));
    }
    if (etag.empty() && last_modified.empty()) {
      std::cout << "[CACHE] etag and last modified is empty" << std::endl;
      throw "etag and Last-Modified are all empty!";
    }
    std::cout << "[CACHE] new header:" << std::endl;
    std::cout << request.getHttpMsg().data() << std::endl;
  }

  void receive_and_forward_with_cache(Request& request, std::string url, int http_server_fd, int client_browser_fd, int request_uid) {
    send_request_with_cache(http_server_fd, request, request_uid);
    std::cout << "[CACHE] receive and forward the response" << std::endl;
    Response new_response;
    try {
      new_response = recv_response_with_cache(http_server_fd, client_browser_fd, request_uid);
    } catch(const char * e) {
      close(client_browser_fd);
    }

    Logger::getLogger()->logRecvResponse(request_uid, request, new_response);

    std::cout << "[CACHE] check if the status code is 304." << std::endl;
    std::cout << "[CACHE] status code: " << new_response.getStatusCode() << std::endl;
    if (new_response.getStatusCode() == 304) {
      std::cout << "[CACHE] received 304." << std::endl;
    } else {
      cache.updateCache(url, new_response);
    }
    Response response = cache.getResponse(url);
    send_response(client_browser_fd, response, request_uid);

  }



  void send_request_with_cache(int http_server_fd, Request request, int request_uid) {
    std::cout << "[CACHE] send the request from proxy to remote server." << std::endl;
    std::cout << "[CACHE] the request sent from proxy is: " << std::endl;
    Logger::getLogger()->logSendRequest(request_uid, request);
    for (std::vector<char> req : request.getHttpMsg()) {
      req.push_back('\0');
      std::cout << req.data() << std::endl;
      req.pop_back();
      send(http_server_fd, req.data(), req.size(), 0);
    }
  }

  void send_response(int client_browser_fd, Response response, int request_uid) {
    int i = 0;
    Logger::getLogger()->logRespondClient(request_uid, response);
    for (vector<char> vec : response.getHttpMsg()) {
      i++;
      std::cout << "[CACHE] sent data to client browser: \n" << std::endl;
      std::cout << vec.data() << std::endl;
      send(client_browser_fd, vec.data(), vec.size(), 0);
//      std::cout << "i: " << i << ", size: " << vec.size() << std::endl;
    }
    close(client_browser_fd);
  }

  bool try_update(std::string url, Response response) {
    std::cout << "[CACHE] try to update the cache" << std::endl;
    if (response.isCacheable()) {
      std::cout << "[CACHE] The response is cacheable" << std::endl;
      cache.updateCache(url, response);
      return true;
    }
    std::cout << "[CACHE] The response is not cacheable" << std::endl;
    return false;
  }

  Response recv_response_with_cache(int http_server_fd, int client_browser_fd, int request_uid) {
    std::cout << "[CACHE] receive response from server with cache" << std::endl;
    std::vector<char> response_header(HEADER_LENGTH);
    int size = recv(http_server_fd, response_header.data(), HEADER_LENGTH, 0);
    response_header.resize(size);
    Response response(response_header);
    if (response.getStatusCode() == 200) {
      Logger::getLogger()->log200_ok(request_uid, response);
    }
    std::cout << "[CACHE] header recv: \n" << std::endl;
    std::cout << response_header.data() << std::endl;
    if (size == 0) {
      close(http_server_fd);
      return response;
    } else if (size == -1) {
      close(http_server_fd);
      throw "error in recv response with cache!";
    }
//    send(client_browser_fd, response_header.data(), size, 0);
    while (true) {
      std::vector<char> response_body(2048);
      std::cout << "[CACHE] receiving body......" << std::endl;
      response_body.clear();
      std::cout << "[CACHE] clear body......" << std::endl;
      size = recv(http_server_fd, response_body.data(), 2048, 0);
      std::cout << response_body.data() << size << std::endl;
      std::cout << "[CACHE] ------>size: " << size << std::endl;
      if (size < 0) {
        close(http_server_fd);
        return response;
      } else if (size == 0) {
        close(http_server_fd);
        return response;
      }
//      send(client_browser_fd, response_body.data(), size, 0);
      std::cout << "[CACHE] data recv: " << std::endl;
      std::cout << response_body.data() << std::endl;
      response.addNewMsg(std::vector<char>(response_body.begin(), response_body.begin() + size));
      std::cout << "[CACHE] added new Msg" << std::endl;
      if (response_body[0] == '0') {
        close(http_server_fd);
        return response;
      }
    }
  }

  ~ProxyWorker() {
    Logger * logger = Logger::getLogger();
    delete logger;
  }
};

int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cerr << "Please enter port num." << std::endl;
    return -1;
  }

  ProxyWorker proxy_woker(argv[1]);
//  proxy.init_server();
  proxy_woker.serve();
//  int client_connection_fd = proxy.accept_connection();
//  proxy.handle_request(client_connection_fd);
//
//  char * header = proxy.receive_header(client_connection_fd);
//  Request request(header);
//  std::string hostname = request.getField("Host");
//  if (hostname.empty()) {
//    close(client_connection_fd);
//    std::cerr << "cannot parse host name" << std::endl;
//    return 0;
//  }
//  int http_server_fd = proxy.connect_to_server(hostname);
//  proxy.forward_to_server(header, http_server_fd);
////  proxy.receive_and_forward_chunked();
////  proxy.receive_from_server();
////  proxy.forward_to_client();
//  proxy.receive_and_forward(http_server_fd, client_connection_fd);
////  proxy.receive_and_forward_directly(http_server_fd, client_connection_fd);
  return 0;
}
