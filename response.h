#ifndef __RESPONSE_H__
#define __RESPONSE_H__

#include "httpmsg.h"
#include "utils.h"
#include <string>
#include <iostream>
#include <stdlib.h>
#define oneHour 3600

using namespace std;

class Response:public HttpMsg{
private:
	int statusCode = -1;
	string contentLen_str;
	time_t getCurTime() const{
		time_t curTime = time(NULL)+5*oneHour;
		return curTime;
	}
public:
	Response(){}
	Response(const vector<char> & first_msg):HttpMsg(first_msg){
 		this->contentLen_str = getField("Content-Length");	
		vector<string> v;
		splitStr(firstLine," ",v);
		this->statusCode = stoi(v[1]);
	}
  bool getContentLen(int & content) const{
		if(0==contentLen_str.size()){
			return false;
		}else{
			content = atoi(contentLen_str.c_str());
			return true;
		}
  }
	bool isVerifiable() const{
		bool isVerifiable = getField("Etag").size()!=0||getField("Last-Modified").size()!=0;
		return isVerifiable;
	}

	bool mustReValidate() const{
		string cacheControl = getField("Cache-Control");
		return cacheControl.size()!=0&&cacheControl.find("no-cache")!=string::npos;
	}

	int getStatusCode() const{
		return statusCode;
	}
	time_t getArriveTime() const{
		string dateStr = getField("Date");
		size_t time_pos = dateStr.find(" GMT");
		string date = dateStr.substr(0,time_pos);
		tm temp;
		strptime(date.c_str(), "%a, %d %b %Y %H:%M:%S", &temp);
		time_t arrive_time = mktime(&temp);
		return arrive_time;	

	}
	bool isFresh() const{//TODO handle time characteristic, what kind of characteristic do we need here 
		//get arrival time
		time_t arrive_time = getArriveTime();
		string cache_control = getField("Cache-Control");
		string name = "max-age=";
		size_t start_pos = cache_control.find(name);
		
		if(start_pos!=string::npos){//first try to use max-age field
			//get max-age, which is lifetime for a request in secs
			start_pos += name.size();
			size_t end_pos = cache_control.find(",");
			string age_str = (end_pos==string::npos)?cache_control.substr(start_pos):cache_control.substr(start_pos,end_pos-start_pos);
			int max_age = stoi(age_str);
			time_t ddl_time = arrive_time+max_age;
			time_t cur_time = getCurTime();
			if(difftime(ddl_time,cur_time)>0){
				return true;	
			}else{
				return false;//if cannot validate, we view that as a sign of fresh
			}	
		}else if(getField("Expires")!=""){//second, try to use expires field
				return true;//TODO when have suitable test case, change this logic	
		}else if(getField("Last-Modified")!=""){//third try to use last modify field
				return true;//TODO when have suitable test case, change this logic
		}else {//if all three doesn't have, assume this response is always fresh	
			return true;//TODO, check whether this logic is correct
		}
	}
	//TODO may add new logic
	bool isCacheable() const{
		string cacheControl = getField("Cache-Control");
		if(cacheControl.size()!=0&&cacheControl.find("no-store")!=string::npos){
			return false;
		}else{
			return true;
		}
	}	
};

#endif
