#ifndef __LOGGER_HPP__
#define __LOGGER_HPP__

#include <iostream>
#include <fstream>
#include <string>
#include <mutex>

#include "request.h"
#include "response.h"

//TODO check the scope of this variable
std::mutex log_mtx;

class Logger
{
private:
	//pointer to the instance 
	static Logger* log_ptr;
	//target file 
	static const std::string log_file;
	//log file stream 
	static ofstream file_stream;
	//id for every request, it records the current largets one 
	static int id;

	Logger(){}
	
public:
	static Logger* getLogger(){
		std::unique_lock<std::mutex> lck(log_mtx);
    if (NULL == log_ptr){
        log_ptr = new Logger();
        file_stream.open(log_file.c_str(), ios::out | ios::app);
    }
    return log_ptr;
	}

	//whenever a new request arrives at our proxy, call this method
	int logNewReq(const Request & request){
		std::unique_lock<std::mutex> lck(log_mtx);
		id++;
		time_t curTime = time(NULL);
		file_stream <<id<<": " <<request.getFirstLine()<<" from "<<request.getHostName()<<" @ "<<asctime(gmtime(&curTime))<<flush;
		return id;
	}
	//whenver a target response of get request is inside our cache, call this method 
	void logNotInCache(const int& cur_id){
		std::unique_lock<std::mutex> lck(log_mtx);
		file_stream <<cur_id<<": " <<"not in cache"<<endl;
	}
	//whenver a target response of get request is not inside our cache, call this method
	void logInCache(const int & cur_id,const Response & response){
		std::unique_lock<std::mutex> lck(mtx);
		file_stream<<cur_id<<": "<<"in cache,";
		if(response.isFresh()){
			time_t arriveTime  = response.getArriveTime(); 	
			file_stream<<" but expired at "<<asctime(gmtime(&arriveTime));	
		}else if(response.mustReValidate()){
			file_stream<<" requires validation"<<endl;
		}else{
			file_stream<<"valid"<<endl;
		}
	}
	//when recieves a 200-ok in response to a GET request, call this method
	void log200_ok(const int & cur_id, Response & response){
		std::unique_lock<std::mutex> lck(mtx);
		file_stream<<cur_id<<": ";
		if(!response.isCacheable()){
			 file_stream<<"not cacheable because there is no-store field inside Cache-Control field"<<endl;
		}else if(response.mustReValidate()){
			file_stream<<"cached, but requires re-validation"<<endl;
		}else {//TODO this logic might need to be changed and print out result
			file_stream<<"cached, will expire in the future"<<endl;
		}
	}
	//when proxy send a request to server, call this method
	void logSendRequest(const int& cur_id,Request & request){
		std::unique_lock<std::mutex> lck(mtx);
		file_stream<<cur_id<<": "<<"Requesting "<<request.getFirstLine()<<" from "<<request.getHostName()<<endl;
	}
	//when proxy receives a respond from server, call this method
	void logRecvResponse(const int & cur_id,Request & request,Response & response){
		std::unique_lock<std::mutex> lck(mtx);
		file_stream<<cur_id<<": "<<"Recieved "<<response.getFirstLine()<<" from "<<request.getHostName()<<endl;
	}	

	//when respond to a client, call this method
	void logRespondClient(const int & cur_id, Response & response){
		std::unique_lock<std::mutex> lck(mtx);
		file_stream<<cur_id<<": Responding "<<response.getFirstLine()<<endl;
	}

	//when close a tunnel, call this method	
	void logCloseTunnel(int cur_id){
		std::unique_lock<std::mutex> lck(mtx);
		file_stream<<cur_id<<": Tunnel closed"<<endl;
	}	
};


Logger* Logger::log_ptr = NULL;

//const string Logger::log_file = "/var/log/erss/proxy.log";
const string Logger::log_file = "./proxy.log";

ofstream Logger::file_stream;
int Logger::id = 0;
#endif
