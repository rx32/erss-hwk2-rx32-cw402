
#include <utility>
#include <thread>

#include "server.cpp"
#include "client.cpp"
#include <iostream>
#include "response.h"
#include <vector>
using namespace std;
class Proxy : public Server, public Client {

public:
  Proxy(const char * listen_port, std::string hostname) : Server(listen_port), Client(std::move(hostname)) {}
	//proxy send to target fd, return the size of date sent in bytes
  int send_all(const std::string& data, int target_fd) {
    int sent_size = 0;
		//cout<<"data inside sendAll is:-------"<<endl;
    //cout<<data;
    //cout<<"---------------------"<<endl;
    const char * request_c_str = data.c_str();
    int request_size = strlen(request_c_str);
    while (sent_size != request_size) {
      int tmp_size = send(target_fd, request_c_str + sent_size, request_size - sent_size, 0);
    	if(-1==tmp_size){
				//cout<<"Cannot send!!!!!!!!!!!!"<<endl;
				return -1;
			}
			sent_size += tmp_size;
		}
		return sent_size;
  }
	//header: fd to server
  void forward_header_to_server() {
    std::cout << "------> data forward to server: " << std::endl;
    std::cout << header << std::endl;
    int size = send_all(std::string(header), target_server_fd);
    std::cout << "------> size forward to server: " << size << std::endl;
  }
  //useless
  void forward_to_client() {
    std::cout << "------> data forward to client: " << std::endl;
    std::cout << "-----> data" << std::endl;
    std::cout << received_data << std::endl;
    int size = send_all(received_data, connection_fd);
    std::cout << "------> forward size: " << size << std::endl;
  }
  //working on it 
  int parse_buffer(const std::string& recv_str, bool & seen_head) {
    int idx = 0;
    int chunk_size = 0;
    std::string chunk_data;
    if (!seen_head) {
      idx = recv_str.find("\r\n\r\n");
      idx += 4;

      // append head
      chunk_data += recv_str.substr(0, idx);
      int chunk_size_end_idx = recv_str.find("\r\n", idx);
      chunk_size = std::stoi(recv_str.substr(idx, chunk_size_end_idx - idx),0, 16);

      // append chunk_size
      chunk_data += recv_str.substr(idx, chunk_size_end_idx+2);
      // point to first byte of data
      idx = chunk_size_end_idx + 2;
    }

    if (DEBUG) {
      std::cout << "Waiting for connection on port " << port << std::endl;
    }

    return 0;
  }

  //recieve from server and forward it to client
  //chunk type
  int receive_and_forward_directly() {
    char buffer[65536];
    while (1) {
      memset(buffer, 0, 65536);
      std::cout << "sending--------------------------------------" << std::endl;
      int size = recv(target_server_fd, buffer, sizeof(buffer), 0);
      std::string recv_str(buffer, size);
      int crlf = recv_str.find("\r\n");
      //used to break out 
      int data_size = recv_str.find("\r\n", crlf+2) - crlf;
      std::cout << "data_size: " << data_size << std::endl;
      if (data_size == 0) {
        break;
      }
      std::cout << "buffer: " << std::string(buffer, size) << std::endl;
      std::cout << "finish sending--------------------------------------" << std::endl;
      send(target_server_fd, buffer, size, 0);
    }
    return 0;
  }
 
  //receive content length from header and forward it to client
  void receive_and_forward_content(){
    std::cout<<endl<<"Start forwarding content type!!!"<<endl;
		char buffer[65536];
    int contentLen = 999;
    int i = 0;
    int curSize = 0;
    while(true){
    	memset(buffer, 0, 65536);
			int tmpSize = recv(target_server_fd,buffer,sizeof(buffer),0);
			if(0==i){//set content length and update current size
        string str(buffer);
        std::cout<<"-------------------------------"<<endl;
				Response response(buffer);
        std::cout<<"str is:"<<endl;
        std::cout<<str<<endl;
        std::cout<<"-------------------------------"<<endl;
				bool isContent = response.getContentLen(contentLen);
        cout<<"content length now is "<<contentLen<<endl;
        if(!isContent){
					throw "This is not content!";
				}
        curSize += (tmpSize-response.getDataPos());
        cout<<"first round data size is "<< (tmpSize-response.getDataPos())<<endl;
      }else{//update current size
				curSize += tmpSize;
			}
      cout<<"this is "<<i<<" round!!!successfully receive header"<<endl;
			cout<<"curSize is "<<curSize<<endl;
			cout<<"contentLen is "<<contentLen<<endl;
      send(connection_fd,buffer,tmpSize,0);
			if(curSize==contentLen) {
				break;
			}
			i++;
    }
    cout<<"finish sending"<<endl;
  }
	//set up tunnel for connection request
	void set_tunnel(){
		//accpet connect from client
  	accept_connection();
		//receive connect request from client
  	cout<<"successfully accpet connection!"<<endl;
		receive_header();
    cout<<"successfully receive header"<<endl;
		//connect to server
  	int status = connect_to_server();//establish connection to server
 		if(-1==status){
			throw "Cannot connect to server";
		}

	}
	//run connection until tunnel is shut down
  void run_connection(){
    cout<<"successfully connect to server"<<endl;
    //send 200 ok to client
    const char * success_msg  = "200 ok";
    size_t size = strlen(success_msg)+1;
    send(connection_fd,success_msg,size,0);
    cout<<"start running connection!"<<endl;
		//set vector for all file descriptors
		vector<int> fds;
    fds.clear();	
		fds.push_back(connection_fd);
		fds.push_back(target_server_fd);
		int max_fd = fds[0]>fds[1]?fds[0]:fds[1];
		while(true){//break if we receive an empty msg
			//prepare the buffer we need
			char buffer[65536];
			memset(buffer,'\0',sizeof(buffer));
      cout<<"In the begining buffer is "<<buffer<<endl;
			//set the readfds we need
			fd_set readfds;
			FD_ZERO(&readfds);
      FD_SET(connection_fd, &readfds);
			FD_SET(target_server_fd, &readfds);
			select(max_fd+1, &readfds, NULL, NULL, NULL);
			//check which file descriptor receive the message
			for(size_t i=0;i<fds.size();i++){
        size_t send_fd = (0==i)?fds[1]:fds[0];
				if(FD_ISSET(fds[i], &readfds)){
					int msg_len = recv(fds[i],buffer,sizeof(buffer),0);
					string msg(buffer);
					if(0==i){
						cout<<"Here I receive a msg from client, msg is:"<<endl;
						cout<<msg<<endl;	
					}else{
						cout<<"Here I receive a msg from server, msg is:"<<endl;
						cout<<msg<<endl;
					}		
					if(0==msg_len){
						cout<<"here I close this tunnel"<<endl;
						return;
					}else{
						cout<<"here I send the message"<<endl;
						send(send_fd,buffer,msg_len,0);
					}
				}	
			}
		}	
	}

  int receive_and_forward_chunked() {
    char buffer[65536];
    memset(buffer, 0, 65536);
    bool seen_head = false;

    int size = recv(target_server_fd, buffer, sizeof(buffer), 0);
    std::string recv_str(buffer, size);
    std::cout << recv_str << std::endl;
    parse_buffer(recv_str, seen_head);
//    while (true) {
//      int size = recv(target_server_fd, buffer, sizeof(buffer), 0);
//      std::string recv_str = std::string(buffer, size);
//      int chunk_begin = 0;
//      int crlf_idx = 0;
//      int chunk_size = 0;
//
//      std::string chunck_data;
//      if (!seen_head) {
//        crlf_idx = recv_str.find("\r\n\r\n");
//        chunck_data += recv_str.substr(crlf_idx + 4);
//        int end_chunk_size = recv_str.find("\r\n", crlf_idx + 4);
//        chunk_size = std::stoi(recv_str.substr(crlf_idx+4, end_chunk_size - (crlf_idx + 4)));
//        chunk_begin = end_chunk_size + 2;
//        crlf_idx = end_chunk_size;
//        seen_head = true;
//      } else {
//        crlf_idx = recv_str.find("\r\n");
//        chunk_size = std::stoi(recv_str.substr(0, crlf_idx));
//      }
//
//      while (chunk_size > 0) {
//        chunck_data += recv_str.substr(chunk_begin, chunk_size);
//        int size = send_all(chunck_data, connection_fd);
//        chunk_begin = crlf_idx + 2;
//        std::cout << "-----> recv a trunk: " << std::endl;
//        std::cout << "-----> recv trunk size: " << chunk_size << std::endl;
//        std::cout << "-----> sent size: " << size << std::endl;
//        std::cout << chunck_data << std::endl;
//
//        int end_chunk_size = recv_str.find("\r\n", crlf_idx + 2);
//        if (end_chunk_size == -1) {
//          break;
//        }
//
//        chunk_size = std::stoi(recv_str.substr(crlf_idx + 2, end_chunk_size - (crlf_idx + 2)));
//        crlf_idx = end_chunk_size;
//      }
//      if (chunk_size == 0) {
//        send_all(recv_str.substr(chunk_begin, crlf_idx+2), connection_fd);
//        break;
//      }
//    }
  return 0;
  }

};
int main(int argc, char *argv[]) {
  if (argc < 3) {
    std::cerr << "Please enter port num." << std::endl;
    return -1;
  }	
  Proxy proxy(argv[1], argv[2]);
  proxy.init_server();
  proxy.set_tunnel();
  proxy.run_connection();
	return 0;
}

/*
int main(int argc, char *argv[]) {
  if (argc < 3) {
    std::cerr << "Please enter port num." << std::endl;
    return -1;
  }
	
  Proxy proxy(argv[1], argv[2]);
  proxy.init_server();
	//two methods below come from server
  proxy.accept_connection();//recieve header from client
  proxy.receive_header();//receive request from client
  //method below comes from client
	cout<<"start connecting to server"<<endl;
  proxy.connect_to_server();//establish connection to server
  //method below comes from proxy
  proxy.forward_header_to_server();//sent request  to server
  //  proxy.receive_and_forward_chunked();
  //  proxy.receive_from_server();
  //  proxy.forward_to_client();
	cout<<"before content forwarding!"<<endl;
  proxy.receive_and_forward_content();//continue receiving data from server and forward it to client
  return 0;
}*/
