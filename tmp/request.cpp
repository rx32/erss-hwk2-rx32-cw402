#include "httpmsg.h"
#include "utils.h"
#include <map>
#include <string>
#include <iostream>
#include <boost/algorithm/string.hpp>

using namespace std;

class Request:public HttpMsg{
private: 
	map<string,string> field_to_val;
	string methodName;
  string reqBody;
  
  //result will contain all lines before request body
  void parseRequest(const string & input){
		const string delimiter = "\r\n";
		vector<string> info;
		info.clear();	

    //iterate through input http request
  	size_t last = 0;
  	size_t next = 0;
  	while ((next = input.find(delimiter, last)) != string::npos) {
    	string sub = input.substr(last, next-last);
    	if(sub.size()!=0){
      	info.push_back(sub);
			}
    	last = next + delimiter.length();
  		if(sub.size()==0){
				break;
			}
		}
		//set request body
		this->reqBody = input.substr(last);
		boost::trim(this->reqBody);
		//parse request line and set header name
    parseReqLine(info[0]);
    //parse header and set a map to present it 
   	parseHeader(info); 
	}
	
	//read in request line, set method name based on it
	void parseReqLine(const string & reqLine){
		int pos = reqLine.find(" ",0);
		if(pos==string::npos){
			string erro = "Invalid request line: "+reqLine;
			throw std::invalid_argument(erro);
		}
		this->methodName = reqLine.substr(0,pos);
	}

	//read in request line and header, use map to represent the header
  void parseHeader(const vector<string> & lines){
		for(int i=1;i<lines.size();i++){
			size_t pos = lines[i].find(":",0);
			if(pos==string::npos){
				string erro = "Invalid line in header: "+lines[i];
				throw invalid_argument(erro);
			  continue;
			}			
			string fieldName = lines[i].substr(0,pos);
			string fieldVal = lines[i].substr(pos+1);
			boost::trim(fieldName);
			boost::trim(fieldVal);
			if(fieldName.size()==0||fieldVal.size()==0){
				string erro = "Invalid line in header: "+lines[i];
				throw invalid_argument(erro);
				continue;
			}
			field_to_val[fieldName] = fieldVal;
		}
  }

public:
	Request(string http):HttpMsg(http){
		parseRequest(http);	
	}
	string getField(string key){
		auto it = field_to_val.find(key);
		if(it==field_to_val.end()){
			cout<<"Cannot find corresponding field"<<endl;
			return "";
		}
		return it->second;
	}
  string getMethodName(){
		return this->methodName;
	}
  string getReqBody()
		return this->reqBody;
	}
};


int main(){
    const char text[] = "GET /uri.cgi HTTP/1.1\r\n"
						            "User-Agent: Mozilla/5.0\r\n"
											  "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n"																		                           "Host: 127.0.0.1:4444\r\n"
												"\r\n";
		string input = text;
		Request req(input);
		cout<<"host is:"<< req.getField("Host")<<endl;
		cout<<"request body is:"<<req.getReqBody()<<endl;
    cout<<"method name is:"<<req.getMethodName()<<endl;
}
