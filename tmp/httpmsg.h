#ifndef _HTTPMSG_H
#define _HTTPMSG_H
#include <string>
using namespace std;
class HttpMsg{
protected: 
	string msg;
public:
  HttpMsg(string http):msg(http){}	
	virtual string getField(string key) = 0; 							
};
#endif 
