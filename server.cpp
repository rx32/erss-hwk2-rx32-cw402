#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "request.h"

#define DEBUG 1
#define HEADER_LENGTH 8192

class Server {

protected:
  const char * listen_port;
  int server_socket_fd;
//  int connection_fd;

protected:
  //TODO: remove global header
  char header[HEADER_LENGTH];


public:

  Server(const char * listen_port): listen_port(listen_port) {}

  ~Server() {
    close(server_socket_fd);
  }

  int init_server() {
    int status;
    struct addrinfo host_info;
    struct addrinfo *host_info_list;
    const char *hostname = nullptr;

    memset(&host_info, 0, sizeof(host_info));

    host_info.ai_family   = AF_UNSPEC;
    host_info.ai_socktype = SOCK_STREAM;
    host_info.ai_flags    = AI_PASSIVE;

    status = getaddrinfo(hostname, listen_port, &host_info, &host_info_list);
    if (status != 0) {
      std::cerr << "Error: cannot get address info for host" << std::endl;
      std::cerr << "  (localhost ," << listen_port << ")" << std::endl;
      return -1;
    }

    server_socket_fd = socket(host_info_list->ai_family,
                       host_info_list->ai_socktype,
                       host_info_list->ai_protocol);
    if (server_socket_fd == -1) {
      std::cerr << "Error: cannot create socket" << std::endl;
      std::cerr << "  (" << hostname << "," << listen_port << ")" << std::endl;
      return -1;
    }

    int yes = 1;
    status = setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
    if (status == -1) {
      std::cerr << "setsockopt failed" << std::endl;
    }

    bind(server_socket_fd, host_info_list->ai_addr, host_info_list->ai_addrlen);

//    status = bind(server_socket_fd, host_info_list->ai_addr, host_info_list->ai_addrlen);

//    if (status == -1) {
//      std::cerr << "Error: cannot bind socket" << std::endl;
//      std::cerr << errno << std::endl;
//      std::cerr << "  (" << hostname << "," << listen_port << ")" << std::endl;
//      return -1;
//    } //if

    freeaddrinfo(host_info_list);

    status = listen(server_socket_fd, 100);
    if (status == -1) {
      std::cerr << "Error: cannot listen on socket" << std::endl;
      std::cerr << "  (" << hostname << "," << listen_port << ")" << std::endl;
      return -1;
    }

    if (DEBUG) {
      std::cout << "Waiting for connection on port " << listen_port << std::endl;
    }

    return 0;
  }

  int accept_connection() {
    struct sockaddr_storage socket_addr{};
    socklen_t socket_addr_len = sizeof(socket_addr);
    int client_browser_fd = accept(server_socket_fd, (struct sockaddr *)&socket_addr, &socket_addr_len);
    if (client_browser_fd == -1) {
      std::cerr << "Error: cannot accept connection on socket" << std::endl;
      return -1;
    }

    if (DEBUG) {
      struct sockaddr_in * sin = (struct sockaddr_in *) &socket_addr;
      const char * connection_ip = inet_ntoa(sin->sin_addr);
      uint16_t connection_port = ntohs(sin->sin_port);
      std::cout << "accept connection from: " << std::endl;
      std::cout << "connection ip: " << connection_ip << std::endl;
      std::cout << "connection port: " << connection_port << std::endl;
    }
    return client_browser_fd;
  }

  char * receive_header(int connection_fd) {
    int size = recv(connection_fd, header, sizeof(header), 0);
    if (size == 0) {
      std::cout << "-----> socket close" << std::endl;
      throw "socket close";
    }
    header[size] = '\0';
//    for (int recv_size = 0; recv_size < sizeof(header); ) {
//      int single_recv_size = recv(connection_fd, header+recv_size, sizeof(header)-recv_size, 0);
//      if (single_recv_size == -1) {
//        break;
//      }
//      recv_size += single_recv_size;
//    }

    std::cout << "received header " << std::endl;
    std::cout << header << std::endl;

//    char response[] = "HTTP/1.1 200 OK\r\nContent-Length: 13\r\nConnection: close\r\n\r\nHello, world!";
//    for (int sent = 0; sent < sizeof(response); sent += send(connection_fd, response+sent, sizeof(response)-sent, 0));
    return header;
  }

};

