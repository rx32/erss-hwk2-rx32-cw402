#ifndef __HTTP_MSG_H__
#define __HTTP_MSG_H__

#include <string>
#include <vector>
#include <unordered_map>
#include <boost/algorithm/string.hpp>
#include "utils.h"

using namespace std;
class HttpMsg{
protected: 
	unordered_map<string,string> field_to_val;
  string firstLine;
	size_t dataPos;
 	vector<vector<char>> httpMsg; 
	//used to parse http 
	void parseFirstMsg(){
		//firstMsg is first part of http response we recieve, however if there are any \0 inside body, we will lose some data
		//however in this function, we only care about data before the real body, so we can work with this variable
		//therefore, we should only use httpMsg if we want to get or send the whole message
		string firstMsg(httpMsg[0].data());
    const string delimiter = "\r\n";
    vector<string> info;
    info.clear();
    //iterate through input http msg
    size_t last = 0;
    size_t next = 0;
    while ((next = firstMsg.find(delimiter, last)) != string::npos) {
      string sub = firstMsg.substr(last, next-last);
      if(sub.size()!=0){
        info.push_back(sub);
      }
      last = next + delimiter.length();
      if(sub.size()==0){
        break;
      }
    }
    this->dataPos = last;
    //set first line 
    this->firstLine = info[0];
    //parse header and set a map to present it
    parseHeader(info);
  }
  //read in first line and headers, use map to represent the header
  void parseHeader(const vector<string> & lines){
    for(size_t i=1;i<lines.size();i++){
      size_t pos = lines[i].find(":",0);
      if(pos==string::npos){
        string erro = "Invalid line in header: "+lines[i];
        throw invalid_argument(erro);
        continue;
      }
      string fieldName = lines[i].substr(0,pos);
      string fieldVal = lines[i].substr(pos+1);
      boost::trim(fieldName);
      boost::trim(fieldVal);
      if(fieldName.size()==0||fieldVal.size()==0){
        string erro = "Invalid line in header: "+lines[i];
        throw invalid_argument(erro);
        continue;
      }
      field_to_val[fieldName] = fieldVal;
    }
  }
public: 
  HttpMsg(const vector<char> & first_msg):httpMsg(){
		httpMsg.clear();
		httpMsg.push_back(first_msg);
		parseFirstMsg();
	}	
	HttpMsg(){} 
  string getField(const string & key) const {
    if(field_to_val.find(key)==field_to_val.end()){
			return "";
		}else{
			return field_to_val.at(key);
		}
  }

	size_t getDataPos() const{
		return this->dataPos;
	}

	void addNewMsg(const vector<char> & newMsg){
		httpMsg.push_back(newMsg);
	}
	vector<vector<char>> getHttpMsg(){
		return httpMsg;
	}
	string getFirstLine() const{
		return firstLine;
	}	
};
#endif 
