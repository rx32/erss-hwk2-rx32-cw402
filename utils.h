#ifndef __UTIL_H__
#define __UTIL_H__


#include <vector>
#include <string>
#include <sstream> 

using namespace std;

void splitStr(const string & input,const string & delimiter,vector<string> & res){
	res.clear();
	size_t last = 0; 
	size_t next = 0; 
	while ((next = input.find(delimiter, last)) != string::npos) { 	
		string sub = input.substr(last, next-last);
		if(sub.size()!=0){
			res.push_back(sub);
		}
		last = next + delimiter.length();
 	} 
	res.push_back(input.substr(last));
}

#endif
