#ifndef __REQUEST_H__
#define __REQUEST_H__

#include "httpmsg.h"
#include "utils.h"
#include <map>
#include <string>
#include <iostream>
#include <boost/algorithm/string.hpp>

using namespace std;

class Request:public HttpMsg{
private: 
	string methodName;
  string url;
	string httpVersion;
	string hostName;
	string port;
	void parseHost(){
		string Host = getField("Host");
		size_t pos = Host.find(":",0);
		if(string::npos!=pos){
			this->hostName = Host.substr(0,pos);	
			this->port = Host.substr(pos+1);	
		}else{
			this->hostName = Host;
			this->port = "80";	
		}
	}
	void parseFirstLine(const string & firstLine){
    vector<string> v;
		splitStr(firstLine," ",v);
		this->methodName = v[0];
		this->url = v[1];
    this->httpVersion = v[2];
	}
public:
	Request(){}
	Request(const vector<char> & first_msg):HttpMsg(first_msg){
		parseFirstLine(this->firstLine);    
  	parseHost();	
	}
	string getMethodName() const{
		return this->methodName;
	}
 	string getHostName() const{
		return this->hostName;
	}
	string getPort() const{
		return this->port;
	}
	string getUrl() const{
		return this->url;
	}
	//TODO check whether this method havn't been tested yet
  void addHeader(string key, string value){
		string curKv = key+": "+value+"\r\n";
		vector<char> first_msg = httpMsg[0];
		size_t pos = 0;
		while(first_msg[pos]!='\r'&&first_msg[pos+1]!='\n'){
			pos++;
		}
		pos += 2;
		for(size_t i=0;i<curKv.size();i++){
			first_msg.insert(first_msg.begin()+pos+i,curKv[i]);			
		}
		httpMsg[0] = first_msg;	
	}
};

#endif

